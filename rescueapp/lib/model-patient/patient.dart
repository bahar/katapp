import 'package:rescueapp/database/database_provide.dart';

class Patient {
  int id;
  String name;
  String birthday;
  bool isAmbilatory;
  String imagePath;
  String category;


  Patient({this.id, this.name, this.birthday, this.isAmbilatory, this.imagePath, this.category});
//converts to map
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_NAME: name,
      DatabaseProvider.COLUMN_BIRTHDAY: birthday,
      DatabaseProvider.COLUMN_ISAMBILATORY: isAmbilatory ? 1 : 0,
      DatabaseProvider.COLUMN_IMAGEPATH: imagePath,
      DatabaseProvider.COLUMN_CATEGORIE: category,

    };

    if (id != null) {
      map[DatabaseProvider.COLUMN_ID] = id;
    }

    return map;
  }

  Patient.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_ID];
    name = map[DatabaseProvider.COLUMN_NAME];
    birthday = map[DatabaseProvider.COLUMN_BIRTHDAY];
    isAmbilatory = map[DatabaseProvider.COLUMN_ISAMBILATORY] == 1;
    imagePath = map[DatabaseProvider.COLUMN_IMAGEPATH];
    category = map[DatabaseProvider.COLUMN_CATEGORIE];

  }
}