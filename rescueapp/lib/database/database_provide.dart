
import 'package:path/path.dart';
import 'package:rescueapp/model-patient/patient.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

//Einige asynchronous Operationen sind:
// Daten über ein Netzwerk abrufen
// Eine Datenbank zu schreiben
//Asynchronisierung
// Daten aus einem File lesen: Async means that this function is asynchronous and you might need to wait a bit to get its result.
// Await literally means - wait here until this function is finished and you will get its return value.
class DatabaseProvider {
  static const String TABLE_PATIENT = "patient";
  static const String COLUMN_ID = "id";
  static const String COLUMN_NAME = "name";
  static const String COLUMN_BIRTHDAY = "geburtsdatum";
  static const String COLUMN_ISAMBILATORY = "isGehfähig";
  static const String COLUMN_IMAGEPATH = "imagePath";
  static const String COLUMN_CATEGORIE = "category";

//  Constructor
  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  Future <Database> get database async {
    print("database getter called");

    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, 'patientDB.db'),
      version: 1,
      onCreate: (Database database, int version) async {
        print("Creating patient table");

        await database.execute(
          "CREATE TABLE $TABLE_PATIENT ("
              "$COLUMN_ID INTEGER PRIMARY KEY,"
              "$COLUMN_NAME TEXT,"
              "$COLUMN_BIRTHDAY TEXT,"
              "$COLUMN_ISAMBILATORY INTEGER,"
              "$COLUMN_IMAGEPATH STRING,"
              "$COLUMN_CATEGORIE STRING,"


              ")",
        );
      },
    );
  }

  Future<List<Patient>> getPatients() async {
    final db = await database;

    var patients = await db
        .query(TABLE_PATIENT, columns: [COLUMN_ID, COLUMN_NAME, COLUMN_IMAGEPATH,COLUMN_BIRTHDAY, COLUMN_ISAMBILATORY, COLUMN_CATEGORIE]);

    List<Patient> patientList = List<Patient>();

    patients.forEach((currentPatient) {
      Patient patient = Patient.fromMap(currentPatient);

      patientList.add(patient);
    });

    return patientList;
  }

  Future<Patient> insert(Patient patient) async {
    final db = await database;
    patient.id = await db.insert(TABLE_PATIENT, patient.toMap());
    return patient;
  }

  Future<int> delete(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_PATIENT,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> update(Patient patient) async {
    final db = await database;

    return await db.update(
      TABLE_PATIENT,
      patient.toMap(),
      where: "id = ?",
      whereArgs: [patient.id],
    );
  }
}