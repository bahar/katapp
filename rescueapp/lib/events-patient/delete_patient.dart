import 'package:rescueapp/events-patient/patient_event.dart';

class DeletePatient extends PatientEvent {
  int patientIndex;

  DeletePatient(int index) {
    patientIndex = index;
  }
}